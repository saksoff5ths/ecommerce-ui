import {createRouter, createWebHashHistory} from 'vue-router'
import HomePage from "@/views/HomePage";
import CategoryPage from "@/views/CategoryPage";

const routes = [
    {
        path: "/",
        component: HomePage
    },
    {
        path: "/category/:id?",
        component: CategoryPage,
        name: "CategoryDetail"
    }
];

const router = createRouter({
    linkActiveClass: 'open active',
    // scrollBehavior: () => ({y: 0}),
    history: createWebHashHistory(),
    routes
})

export default router