export const API_SERVER = 'http://localhost:8080'
export const BASE_URL = '/api/v1/customer'

export const USER = 'user'
export const EXPIRES_IN = 'exp_in'
export const AUTHORIZATION = 'authorization'
export const REFRESH_TOKEN = 'ref_t'
