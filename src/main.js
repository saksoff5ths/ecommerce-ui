import {createApp} from 'vue'
import VueAxios from 'vue-axios'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import http from './plugins/axios'
import {loadFonts} from './plugins/webfontloader'
import router from "./router"

loadFonts()

createApp(App)
    .use(vuetify)
    .use(router)
    .use(VueAxios, http)
    .mount('#app');

